import math

from docplex.mp.model import Model
from numpy.random import randint
import cplex
import time


def create_and_run_model(distance_between_test_points=12, data_rate_requirement_for_stp=25.2, original_pl_model=True,
                         data_rate_requirement_for_users=75.6, number_of_floors=3, distance_between_users=10,
                         time_limit=300):
    model = Model('femtocell_placement')
    model.time_limit = time_limit

    class Site:
        """
        Potential placement for FBS
        """

        def __init__(self, x, y, floor, id):
            self.x = x
            self.y = y
            self.floor = floor
            self.power = 5  # dBm
            self.height = 2  # m
            self.frequency = 2600000000  # Hz
            self.bandwidth = 20000000  # Hz
            self.capacity = 32
            self.id = id

    class STP:
        """
        Signal Test Point
        """

        def __init__(self, x, y, floor, id):
            self.x = x
            self.y = y
            self.floor = floor
            self.height = 0.8  # m
            self.data_rate_requirement = 25200000  # b/s
            self.id = id

    class User:

        def __init__(self, x, y, floor, id):
            self.x = x
            self.y = y
            self.floor = floor
            self.height = 0.8  # m
            self.data_rate_requirement = 75600000  # b/s
            self.id = id

    def distance(p1, p2):
        return ((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2 + ((p1.floor - p2.floor) * 3) ** 2) ** 0.5

    def path_loss(r, n, q):
        """
        This function calculates path loss
        :param r: distance between Tx and Rx in meters
        :param n: number of building's floor (I try to change it to floor difference)
        :param q: number of wall separation of FBS and STPs or user
        :return: path loss in dB
        """
        if not r:
            return 0
        d = 0.18  # wall thickness
        Liw = 2.94  # wall loss
        if original_pl_model:
            pl = 40.7412 + 20 * math.log10(r) + 0.7 * d + 18.3 ** ((n + 2) / (n + 1) - 0.46) + q * Liw
        else:
            pl = 40.7412 + 20 * math.log10(r) + 0.7 * d + 10 * n + q * Liw
        return pl

    def id_generator():
        i = 0
        while True:
            yield i
            i += 1

    # -----------------------------------------------------------------------------------------------------------------
    # ---------------------------------------------------Constants-----------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------

    sites = []
    generator = id_generator()
    for floor in range(1, number_of_floors + 1, 1):
        for x in range(6, 75, 6):
            for y in range(6, 75, 6):
                site = Site(x=x, y=y, floor=floor, id=next(generator))
                sites.append(site)

    stps = []
    generator = id_generator()
    for floor in range(1, number_of_floors + 1, 1):
        for x in range(distance_between_test_points, 75, distance_between_test_points):
            for y in range(distance_between_test_points, 75, distance_between_test_points):
                stp = STP(x=x, y=y, floor=floor, id=next(generator))
                stps.append(stp)

    users = []
    generator = id_generator()
    for floor in range(1, number_of_floors + 1, 1):
        for x in range(distance_between_users, 75, distance_between_users):
            for y in range(distance_between_users, 75, distance_between_users):
                user = User(x=x, y=y, floor=floor, id=next(generator))
                users.append(user)

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------Decision variables -----------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------

    #  Is FBS installed at site a
    model.Ba = model.binary_var_dict((site for site in sites),
                                     name=lambda item: "B%s" % item.id)

    #  Is STP j assigned to FBS a
    model.Sja = model.binary_var_dict(((stp, site) for stp in stps for site in sites),
                                      name=lambda item: "S%s_%s" % (item[0].id, item[1].id))

    #  Is user i assigned to FBS a
    model.Uia = model.binary_var_dict(((user, site) for user in users for site in sites),
                                      name=lambda item: "U%s_%s" % (item[0].id, item[1].id))

    # -----------------------------------------------------------------------------------------------------------------
    # ----------------------------------------------Constraint parameters----------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------

    # The signal strength that a STP j receives from FBS a, j belongs to set of STP and a belongs to candidate sites

    P = {}
    for fbs in sites:
        P[fbs] = {}

        for stp in stps:
            r = distance(p1=fbs, p2=stp)
            if original_pl_model:
                P[fbs][stp] = 5 - path_loss(r=r, n=number_of_floors, q=randint(math.floor(r / 10), math.floor(r / 5) + 1))
            else:
                P[fbs][stp] = 5 - path_loss(r=r, n=abs(stp.floor - fbs.floor), q=randint(math.floor(r / 10), math.floor(r / 5) + 1))

        for user in users:
            r = distance(p1=fbs, p2=user)
            if original_pl_model:
                P[fbs][user] = 5 - path_loss(r=r, n=number_of_floors, q=randint(math.floor(r / 10), math.floor(r / 5) + 1))
            else:
                P[fbs][user] = 5 - path_loss(r=r, n=abs(user.floor - fbs.floor), q=randint(math.floor(r / 10), math.floor(r / 5) + 1))

    # The received signal strength threshold for STPs

    Pt = {}
    for stp in stps:
        if data_rate_requirement_for_stp == 25.2:
            Pt[stp] = -92.93  # QPSK 3/4

    # The received signal strength threshold for users

    Pu = {}
    for user in users:
        if data_rate_requirement_for_users == 33.6:
            Pu[user] = -90.53  # 16 QAM 1/8
        elif data_rate_requirement_for_users == 50.4:
            Pu[user] = -86.23  # 16 QAM 3/4
        elif data_rate_requirement_for_users == 67.2:
            Pu[user] = -83.13  # 64 QAM 2/3
        elif data_rate_requirement_for_users == 75.6:
            Pu[user] = -80.93  # 64 QAM 3/4

    # Maximum capacity of FBS

    Ca = {}
    for fbs in sites:
        Ca[fbs] = 32  # 32 users fpr single FBS

    # -----------------------------------------------------------------------------------------------------------------
    # ---------------------------------------------------Constraints---------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------

    # first condition (2)
    for stp in stps:
        model.add_constraint(model.sum(model.Sja[stp, site] for site in sites) >= 1)

    # second condition (3)
    for site in sites:
        for stp in stps:
            model.add_constraint(model.Sja[stp, site] <= model.Ba[site])

    # third condition (4)
    for stp in stps:
        for site in sites:
            model.add_constraint(model.Sja[stp, site] * (P[site][stp] - Pt[stp]) >= 0)

    # fourth condition (5)
    for user in users:
        model.add_constraint(model.sum(model.Uia[user, site] for site in sites) >= 1)

    # fifth condition (6)
    for site in sites:
        for user in users:
            model.add_constraint(model.Uia[user, site] <= model.Ba[site])

    # sixth condition (7)
    for site in sites:
        for user in users:
            model.add_constraint(model.Uia[user, site] * (P[site][user] - Pu[user]) >= 0)

    # seventh condition (8)
    for site in sites:
        model.add_constraint(model.sum(model.Uia[user, site] for user in users) <= Ca[site])

    model.minimize(model.sum(model.Ba[site] for site in sites))

    solution = model.solve(log_output=True)

    with open('first_phase_solution.txt', 'w') as f:
        print(solution, file=f)

    print("Sufficient number of Base Stations: ", solution.objective_value)

    #  ---------------------------------------------------------------------------------------------
    #  ------------------------------------Second phase---------------------------------------------

    N_one = solution.objective_value

    #  eighth condition (13)
    model.add_constraint(model.sum(model.Ba[site] for site in sites) <= N_one)

    model.maximize(model.sum(model.Sja[stp, site]*P[site][stp] for stp in stps for site in sites))

    solution = model.solve(log_output=True)

    with open('second_phase_solution.txt', 'w') as f:
        print(solution, file=f)

    print('_______________________________________________________________________\n')
    print("Objective value of second phase:", round(solution.objective_value, 2), end='\n\n')
    print("This value is sum of signal strength in dBm received in all signal test points.\n")
    print("Number of test points is", len(stps), "so mean signal received value is", round(solution.objective_value/len(stps), 2), "dBm, which is reasonable,")
    print("since power threshold for test point is equal to", Pt[stps[0]], "dBm.", end='\n\n')
    print("Active base stations at locations:")
    for bs in solution.model.Ba:
        if model.Ba[bs].solution_value:
            print("x: ", bs.x, ", y: ", bs.y, ", floor: ", bs.floor)


if __name__ == '__main__':
    time_measurements = False
    if time_measurements:
        result_file = open("measurements.txt", "a")
        for users_distance in range(10, 75, 30):
            for no_floors in range(1, 7):
                for iter in range(1, 5):
                    tic = time.perf_counter()
                    create_and_run_model(distance_between_users=users_distance, distance_between_test_points=12,
                                         number_of_floors=no_floors, original_pl_model=True)
                    toc = time.perf_counter()
                    print("Iteration: " + str(iter) +
                          " users distance: " + str(users_distance) +
                          " test point distance: " + str(12) +
                          " floors number: " + str(no_floors) +
                          " time: " + str(toc-tic) + '\n')

                    result_file.write("Iteration: " + str(iter) +
                                      " users distance: " + str(users_distance) +
                                      " test point distance: " + str(12) +
                                      " floors number: " + str(no_floors) +
                                      " time: " + str(toc-tic) + '\n')

        result_file.close()
    else:
        tic = time.perf_counter()
        create_and_run_model(distance_between_users=8, distance_between_test_points=10,
                             number_of_floors=2, original_pl_model=False)
        toc = time.perf_counter()
        print(toc - tic)
